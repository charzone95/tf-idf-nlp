Ani Yudhoyono: Selama Ini Orang Menganggap Agus Anak Ingusan
Ani Yudhoyono, Ibunda dari calon gubernur DKI Jakarta, Agus Harimurti Yudhoyono, mengaku puas dengan performa dari putra sulungnya saat debat cagub dan cawagub yang digagas oleh Komisi Pemilihan Umum DKI Jakarta.
"Tentu saya sebagai ibunya merasa bersyukur dan bangga atas penampilan Agus pada malam hari ini," ujar Ani seusai mengikuti debat tersebut di Hotel Bidakara, Jakarta Selatan, Jumat (13/1/2017).
Ani menganggap, performa anaknya itu dapat membungkam nada-nada sumbang yang menganggap suami Annisa Pohan itu tidak berani berdebat. Agus sempat tidak menghadiri dua agenda debat yang digagas oleh stasiun televisi swasta.
"Selama ini orang mengatakan bahwa Agus anak ingusan. Yang kedua dia tidak berani debat, tapi hari ini, malam ini, dia membuktikan dia berani melakukan perdebatan dengan paslon lainnya," ucap dia.
Istri dari mantan Presiden RI Susilo Bambang Yudhoyono itu berharap agar putra sulungnya dapat memenangi Pilkada DKI 2017 ini.
"Saya sebagai orang tuannya begitu bergembira dan berharap 15 Februari suara kepada Agus. Saya kira itu," kata Ani.
