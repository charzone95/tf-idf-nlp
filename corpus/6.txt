Gaya Plt Gubernur DKI Jakarta saat Kunjungi Borobudur
Tak lama berselang setelah tiba di Yogyakarta, Pelaksana Tugas (Plt) Gubernur DKI Sumarsono beserta rombongan pimpinan satuan kerja perangkat daerah (SKPD) mendatangi objek wisata bersejarah Candi Borobudur, di Magelang, Jawa Tengah, Sabtu (14/1/2017).
Adapun penampilan Soni, sapaan Sumarsono, tampak berbeda ketika mendatangi Candi Borobudur.
Jika kesehariannya di Balai Kota mengenakan baju batik atau baju dinas, kali ini Soni terlihat cukup necis mengenakan kemeja putih merek "Lee Cooper" serta celana jeans biru merek "Levi's".
Kacamata berwarna hitam merek "Porsche Design" menghiasi penampilan Soni. Tak ketinggalan, batu cincin dengan ukiran huruf SS yang melingkar di jari manis.
Adapun rombongan SKPD lainnya membentuk sebuah kelompok kecil saat tiba di candi.  Misalnya, Kepala Satpol PP DKI Jakarta Jupan Royter, Kepala Badan Pengelola Keuangan DKI Jakarta Heru Budi Hartono, serta sejumlah SKPD lainnya yang sedang mengelilingi candi.
Setelah mengelilingi Candi Borobudur, Soni berharap warisan budaya yang ada di Jakarta seperti Kota Tua bisa tetap dirawat seperti Candi Borobudur.
Soni menilai, salah satu alasan mengapa wisatawan datang ke objek wisata dikarenakan sejarah tempat tersebut. Menurut Soni, Kota Tua tak kalah dibanding Borodur ataupun objek wisata lainnya.
"Kalau di Jakarta terkait Batavia, dan seterusnya yang akan kami branding ya Kota Tua. Kami benahi untuk membuat bangunan sejarah menjadi lebih menarik," ujar Soni.
Sabtu malam, Soni dan rombongan pimpinan SKPD dijadwalkan bertemu Gubernur DIY Sri Sultan Hamungkubowono X.
