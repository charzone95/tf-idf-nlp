import sun.reflect.generics.tree.Tree;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ASUS on 1/15/2017.
 */
public class Tfidf {

    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        String corpusDirectoryName = "corpus";

        ArrayList<String> listOfFilename = getAllFilesInDirectory(corpusDirectoryName);
        TreeMap<String, TreeMap<String, Integer>> wordSummary = new TreeMap<>();

        for (String filename : listOfFilename) {
            String content = readFile(corpusDirectoryName + "/" + filename);
            ArrayList<String> wordList = extractWords(content);
            TreeMap<String, Integer> summary = summarizeWord(wordList);

            wordSummary.put(filename, summary);
        }

        TreeMap<String, LinkedHashMap<String, Double>> tfIdfData = countTfIdf(wordSummary);
        printWordSummary(wordSummary);
        printTfIdf(tfIdfData);


        input.close();
    }

    private static String cleanWord(String word) {
        String result = word;

        //remove leading chars
        result = result.replaceAll("^[\"'(:“‘]+", "");

        //remove trailing chars
        result = result.replaceAll("[\"'):,.”’]+$", "");

        return result;
    }

    private static String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));

        return new String(encoded, StandardCharsets.UTF_8);
    }

    private static ArrayList<String> extractWords(String fileContent) {
        String[] explodedStrings = fileContent.split("\\s+");

        ArrayList<String> results = new ArrayList<>();
        for (String word : explodedStrings) {
            //clean the word
            word = cleanWord(word);

            //lowercase the word
            word = word.toLowerCase();

            //add to list
            results.add(word);
        }

        return results;
    }

    private static ArrayList<String> getAllFilesInDirectory(String directoryName) {
        ArrayList<String> results = new ArrayList<>();

        File directory = new File(directoryName);
        File[] listOfFiles = directory.listFiles();

        assert listOfFiles != null;
        for (File file : listOfFiles) {
            if (file.isFile()) {
                results.add(file.getName());
            }
        }

        return results;
    }

    private static TreeMap<String, Integer> summarizeWord(ArrayList<String> wordsList) {
        TreeMap<String, Integer> result = new TreeMap<>();

        for(String word : wordsList) {
            if (result.containsKey(word)) {
                //word already in TreeMap, increment value
                result.put(word, result.get(word)+1);
            } else {
                result.put(word, 1);
            }
        }

        return result;
    }

    private static TreeMap<String, LinkedHashMap<String, Double>> countTfIdf(TreeMap<String, TreeMap<String, Integer>> summary) {
        TreeMap<String, LinkedHashMap<String, Double>> result = new TreeMap<>();

        for (Map.Entry<String, TreeMap<String, Integer>> document : summary.entrySet()) {
            LinkedHashMap<String, Double> documentResult = new LinkedHashMap<>();

            for (Map.Entry<String, Integer> word : document.getValue().entrySet()) {
                //count tf
                double tf = (double)word.getValue() / document.getValue().size();

                //check how many document have the term
                int count = 0;
                for (Map.Entry<String, TreeMap<String, Integer>> entry : summary.entrySet()) {
                    if (entry.getValue().containsKey(word.getKey())) {
                        count++;
                    }
                }

                double idf = Math.log((double) summary.size() / count);

                double tfidf = tf*idf;
                documentResult.put(word.getKey(), tfidf);
            }

            //sort tfidf treemap
            documentResult = sortByComparator(documentResult, false);

            result.put(document.getKey(), documentResult);
        }

        return result;
    }

    private static void printWordSummary(TreeMap<String, TreeMap<String, Integer>> summary) {
        for (Map.Entry<String, TreeMap<String, Integer>> document : summary.entrySet()) {
            System.out.println(document.getKey());
            System.out.println("===============");

            for (Map.Entry<String, Integer> word : document.getValue().entrySet()) {
                System.out.println(word.getKey() + ": " + word.getValue());
            }


            System.out.println();
        }
    }

    private static void printTfIdf(TreeMap<String, LinkedHashMap<String, Double>> data) {
        for (Map.Entry<String, LinkedHashMap<String, Double>> document : data.entrySet()) {
            System.out.println(document.getKey());
            System.out.println("===============");

            for (Map.Entry<String, Double> word : document.getValue().entrySet()) {
                System.out.println(word.getKey() + ": " + word.getValue());
            }


            System.out.println();
        }
    }


    // credit to: http://stackoverflow.com/a/13913315
    private static LinkedHashMap<String, Double> sortByComparator(Map<String, Double> unsortedMap, final boolean ascendingOrder)
    {

        List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortedMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>()
        {
            public int compare(Map.Entry<String, Double> o1,
                               Map.Entry<String, Double> o2)
            {
                if (ascendingOrder)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        LinkedHashMap<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}
